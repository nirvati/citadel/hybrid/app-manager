#[cfg(feature = "schema")]
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
#[serde(untagged)]
pub enum Command {
    SimpleCommand(String),
    ArrayCommand(Vec<String>),
}


#[derive(Clone, Deserialize, Serialize, PartialEq, Eq, Debug)]
#[serde(untagged)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
pub enum StringOrIntOrBool {
    String(String),
    Int(i64),
    Bool(bool),
}

#[derive(Clone, Deserialize, Serialize, PartialEq, Eq, Debug)]
#[serde(untagged)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
pub enum StringOrInt {
    String(String),
    Int(u64),
}

#[derive(Clone, Default, Deserialize, Serialize, PartialEq, Eq, Debug)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
pub struct NetworkEntry {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ipv4_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ipv6_address: Option<String>,
}

#[derive(Clone, Default, Deserialize, Serialize, PartialEq, Eq, Debug)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
#[serde(rename = "service")]
pub struct Service {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cap_add: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub command: Option<Command>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub depends_on: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entrypoint: Option<Command>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub environment: Option<BTreeMap<String, StringOrIntOrBool>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extra_hosts: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hostname: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub init: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub network_mode: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub networks: Option<BTreeMap<String, NetworkEntry>>,
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub ports: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restart: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_grace_period: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_signal: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub volumes: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub working_dir: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shm_size: Option<StringOrInt>,
    #[serde(skip_serializing_if = "BTreeMap::is_empty", default)]
    pub labels: BTreeMap<String, StringOrIntOrBool>,
}

#[derive(Clone, Default, Deserialize, Serialize, PartialEq, Eq, Debug)]
#[cfg_attr(feature = "schema", derive(JsonSchema))]
#[serde(rename = "Compose Specification")]
pub struct ComposeSpecification {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub services: Option<BTreeMap<String, Service>>,
}
